// Get dependencies
const express = require('express');
const http = require('http');

//ROUTES
var api = require('./routes/api');

const app = express();

app.use('/v1/api', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 500;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: err
    });
});

const port = process.env.PORT || 3000;
app.set('port', port);

// create HTTP Server
const server = http.createServer(app);

// Listen to provided port
server.listen(port, () => console.log(`API running on localhost:${port}`));
