const express = require('express');
const router = express.Router();

// controllers
var objController = require('../src/index');

// [API ROUTE LIST]
router.get('/', (req, res) => {
    res.send('api works');
 });
router.get('/obj', objController.handler);
// --> END API ROUTE LIST
  
module.exports = router;