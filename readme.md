To run in development mode:
    * change environment variable in config/config.js to 'development'
    * set your objtemplate_id in config/config.js
    * then run command: node index.js within root project folder
    * you can access the project in this URL 127.0.0.1/v1/api/obj

When production mode:
    * remove all public.min.css and global.min.js (search in project folder)


** WHEN COMMITTING/PUSHING CHANGES TO MASTER BRANCH ALWAYS MAKE SURE TO SET THE ENVIRONMENT VARIABLE TO 'PRODUCTION' 
   AS IT WILL TRIGGER THE BITBUCKET PIPELINE AND DEPLOY TO AWS
**
