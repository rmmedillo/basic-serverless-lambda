!function () {
  'use strict';

  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
  }

  if (!Element.prototype.closest) {
    Element.prototype.closest = function (s) {
      var el = this;

      do {
        if (Element.prototype.matches.call(el, s)) return el;
        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);

      return null;
    };
  }

  var hasClass = function hasClass(el, cls) {
    if (el.className.match === undefined) return false;
    return !!el.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
  };

  var addClass = function addClass(el, cls) {
    if (!hasClass(el, cls)) {
      el.className == "" ? el.className = cls : el.className += " " + cls;
    }
  };

  var removeClass = function removeClass(el, cls) {
    if (hasClass(el, cls)) {
      el.className = el.className.replace(new RegExp('(\\s|^)' + cls + '(\\s|$)'), ' ').trim();
    }
  };

  var toggleClass = function toggleClass(el, cls) {
    if (hasClass(el, cls)) removeClass(el, cls);else addClass(el, cls);
  };

  var hasHashTag = function hasHashTag() {
    if (window.location.hash) {
      var hash = window.location.hash.substring(1);

      if (hash.length === 0) {
        return false;
      } else {
        return hash;
      }
    } else {
      return false;
    }
  }

  var renderLoadMore = function renderLoadMore(buttonSelector, storiesSelector, hiddenStoriesSelector, hiddenClass) {
    var _loadMoreButton = document.querySelector(buttonSelector);
    var _allStories = document.querySelectorAll(storiesSelector);
    console.log(_allStories);

    if (_allStories.length > 4) {
      for (var _i4 = 4, _j4 = _allStories.length; _i4 < _j4; _i4++) {
        var parent = _allStories[_i4].closest('.wwf60__column');
        var parent_classes = parent.className;

        if (parent_classes.indexOf(hiddenClass) <= 0) {
          addClass(parent, hiddenClass);
        }
      }

      removeClass(_loadMoreButton, 'wwf60__load-more-button--hidden');
      listen(_loadMoreButton, 'click', function (e) {
        var hiddenStories = document.querySelectorAll(hiddenStoriesSelector);
        // console.log(hiddenStories.length);
        if (hiddenStories.length <= 2) {
          addClass(_loadMoreButton, 'wwf60__load-more-button--hidden');
        }

        var toShow = Array.prototype.slice.call(hiddenStories, 0, 2);

        for (var _i5 = 0, _j5 = toShow.length; _i5 < _j5; _i5++) {
          removeClass(toShow[_i5], hiddenClass);
        }
      });
    }
  }

  /**
    * Bind event listeners
    * useage:
    *
    * let element = document.querySelector('<element selector>');
    * listen(element, 'click', function(event) {
    *   --- code to execute on click---
    * });
    *
    */


  var listen = function listen(el, ev, cb) {
    var uc = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    if (el.addEventListener) el.addEventListener(ev, cb, uc);else el.attachEvent('on' + ev, cb);
  };

  /**
    * Set focus to element
    */


  var setFocus = function setFocus(el) {
    var allowScroll = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var __x = window.scrollX,
        __y = window.scrollY;
    el.focus();
    if (!allowScroll) window.scrollTo(__x, __y);
  };

  var isKey = function isKey(e, k) {
    return "key" in e && e.key == k.key || (e.keyCode ? e.keyCode : e.which) == k.code;
  };

  var Modal = function () {
    var context = new Object();
    var DOM = {
      backdrop: document.querySelector('.js-wwf60-modal-backdrop'),
      modal: document.querySelector('.js-wwf60-modal'),
      trigger: document.querySelectorAll('.js-wwf60-modal-trigger'),
      dismiss: document.querySelectorAll('.js-wwf60-dismiss-modal'),
      content: document.querySelector('.js-wwf60-modal-content-container'),
      content_footer: document.querySelector('.wwf60__modal-footer')
    };
    var inactiveClassModal = 'wwf60__modal--inactive';
    var inactiveClassBackdrop = 'wwf60__modal-backdrop--inactive';
    var keys = {
      enter: {
        key: 'Enter',
        code: 13
      },
      tab: {
        key: 'Tab',
        code: 9
      },
      esc: {
        key: 'Escape',
        code: 27
      }
    };

    var init = function init() {
      if (!DOM.modal) return false;

      for (var i = 0, j = DOM.trigger.length; i < j; i++) {
        listen(DOM.trigger[i], 'click', function (e) {
          return toggle(e, true);
        });
      }

      for (var _i = 0, _j = DOM.dismiss.length; _i < _j; _i++) {
        listen(DOM.dismiss[_i], 'click', function (e) {
          return toggle(e, false);
        });
      }

      listen(document, 'keydown', function (e) {
        if (isKey(e, keys.esc)) {
          close(null);
        }
      });
    };

    var activateTabindex = function activateTabindex() {
      DOM.modal.setAttribute('tabindex', '0');
      var tabs = DOM.modal.querySelectorAll("[tabindex]");

      for (var i = 0, j = tabs.length; i < j; i++) {
        tabs[i].setAttribute('tabindex', '0');
      }
    };

    var deactivateTabindex = function deactivateTabindex() {
      DOM.modal.setAttribute('tabindex', '-1');
      var tabs = DOM.modal.querySelectorAll("[tabindex]");

      for (var i = 0, j = tabs.length; i < j; i++) {
        tabs[i].setAttribute('tabindex', '-1');
      }
    };

    var inject = function inject(content, content_footer) {
      DOM.content.innerHTML = content;
      DOM.content_footer.innerHTML = content_footer;
    };

    var toggle = function toggle(e, open) {
      if (open) { 
        activateTabindex();

        if (!!e) {
          context.lastTrigger = e.target;
        }

        setFocus(DOM.modal, false);
        removeClass(DOM.backdrop, inactiveClassBackdrop);
        removeClass(DOM.modal, inactiveClassModal);
      } else {
        deactivateTabindex();

        if (!!context.lastTrigger) {
          setFocus(context.lastTrigger, false);
          context.lastTrigger = null;
        }

        addClass(DOM.backdrop, inactiveClassBackdrop);
        addClass(DOM.modal, inactiveClassModal);
      }
    };

    var open = function open(e) {
      return toggle(e, true);
    };

    var close = function close(e) {
      return toggle(e, false);
    };

    return {
      init: init,
      open: open,
      close: close,
      inject: inject
    };
  }();

  var DOM = {
    card: '.js-wwf60-card-{{getObjID}}',
    flipButton: '.js-wwf60-flip-button-{{getObjID}}',
    expandButton: '.js-wwf60-expand-button-{{getObjID}}'
  };

  var flipTabindex = function flipTabindex(element) {
    var tab = element.querySelectorAll("[tabindex='0']"),
        notab = element.querySelectorAll("[tabindex='-1']");

    for (var i = 0, j = tab.length; i < j; i++) {
      tab[i].setAttribute('tabindex', '-1');
    }

    for (var _i2 = 0, _j2 = notab.length; _i2 < _j2; _i2++) {
      notab[_i2].setAttribute('tabindex', '0');
    }
  };

  var flipCard = function flipCard(e) {
    var clicked = e.target,
        card = clicked.closest(DOM.card);
    flipTabindex(card);
    return toggleClass(card, 'flip');
  };

  var openModal = function openModal(e) {
    var clicked = e.target,
        card = clicked.closest(DOM.card),
        content = document.querySelector("[data-content-for=\"card-{{getObjID}}-".concat(card.dataset.cardIndex, "\"]")),
        content_footer = document.querySelector("[data-content-for=\"card-footer-{{getObjID}}-".concat(card.dataset.cardIndex, "\"]"));
    Modal.inject(content.innerHTML,content_footer.innerHTML);
    Modal.open(e);
  };

  var flipButtons = document.querySelectorAll(DOM.flipButton);

  for (var i = 0, j = flipButtons.length; i < j; i++) {
    listen(flipButtons[i], 'click', flipCard);
  }

  var expandButtons = document.querySelectorAll(DOM.expandButton);

  for (var _i3 = 0, _j3 = expandButtons.length; _i3 < _j3; _i3++) {
    listen(expandButtons[_i3], 'click', openModal);
  }

  var allStories = document.querySelectorAll('.js-wwf60-card-{{getObjID}}'),
      loadMoreButton = document.querySelector('.js-wwf60-load-more-{{getObjID}}'),
      groupLoadMoreButton = document.querySelector('.group-load-more-{{getObjID}}');

  renderLoadMore(
    '.js-wwf60-load-more-{{getObjID}}',
    '.js-wwf60-card-{{getObjID}}',
    '.js-wwf60__column--hidden-{{getObjID}}',
    'wwf60__column--hidden js-wwf60__column--hidden-{{getObjID}}'
  );

  if (!hasHashTag()) {} else {
    var identifier = hasHashTag();
    var content = document.querySelector("[data-identifier-for=\"card-"+ identifier.toLowerCase() + "\"]"),
        content_footer = document.querySelector("[data-identifier-for=\"card-footer-"+ identifier.toLowerCase() + "\"]");
    Modal.inject(content.innerHTML,content_footer.innerHTML);
    Modal.open();
  }


  $('.dropdown-menu li a').on('click', function(){

    addClass(loadMoreButton, 'wwf60__load-more-button--hidden');
    
    addClass(groupLoadMoreButton, 'wwf60__load-more-button--hidden');
  
    var current = $(this).data('value'); 

    for (var _i4 = 0, _j4 = allStories.length; _i4 < _j4; _i4++) {
      var parent = allStories[_i4].closest(".wwf60__column");
      var parent_classes = parent.className;

      if (current == 'all') {
        removeClass(parent, 'wwf60__column--hidden');
        removeClass(parent, 'js-wwf60__column--hidden-{{getObjID}}');
      } else {
        if (parent.dataset.group != current) {
          if (parent_classes.indexOf('wwf60__column--hidden') <= 0) {
            addClass(parent, 'wwf60__column--hidden');
          }
          if (parent_classes.indexOf('js-wwf60__column--hidden-{{getObjID}}') <= 0) {
            addClass(parent, 'js-wwf60__column--hidden-{{getObjID}}');
          }
        } else if (parent.dataset.group == current) {
          removeClass(parent, 'wwf60__column--hidden');
          removeClass(parent, 'js-wwf60__column--hidden-{{getObjID}}');
        }
      }
    }
    
    if (current == 'all') {
      renderLoadMore(
        '.js-wwf60-load-more-{{getObjID}}',
        '.js-wwf60-card-{{getObjID}}',
        '.hidden-group-{{getObjID}}',
        'wwf60__column--hidden hidden-group-{{getObjID}} js-wwf60__column--hidden-{{getObjID}}'
      );
      
    } else {
      renderLoadMore(
        '.group-load-more-{{getObjID}}',
        '#' + current,
        '.hidden-group-{{getObjID}}-{{current}}',
        'wwf60__column--hidden hidden-group-{{getObjID}}-{{current}} js-wwf60__column--hidden-{{getObjID}}'
      );
    }

  });

  Modal.init();
}();