const environment = "development"; // production, development 

module.exports = {
    renderValues : function (post_data) {
        if (environment == "production") {
            var _post_data = JSON.parse(JSON.stringify(post_data));
        } else {
            var config = require('../config/faker.js');
            var _post_data = config.fake_data;
        }

        var data = 
        {
            "items": _post_data,
            "index": function() {
                return data.items[0].items.card_settings.indexOf(this);
            },
            "getImgURL": function() {
                return function(val, render) {
                    var sXImgID = render(val);
                    if (data.items[0].images.hasOwnProperty(parseInt(sXImgID))) {
                        return data.items[0].imageURL + data.items[0].images[parseInt(sXImgID)].XIMG_FILE;
                    } else {
                        return null;   
                    }   
                }
            },
            "getCTAValues": function() {
                return data.items[0].items.cta_settings;
            },
            "getObjID": function() {
                return data.items[0].obj_id;
            },
            "replaceSpacewithUnderscore": function() {
                return function(val, render) {
                    var str = render(val);
                    str.toString();
                    str = str.toLowerCase();
                    return str.replace(/[^a-zA-Z0-9_-]/g,'_');
                }
            },
            "toLowerCase": function() {
                return function(val, render) {
                    var str = render(val);

                    return str.toLowerCase();
                }
            },
            "getFilterGroup": function() {
                var list = new Array();
                var item_list = {};
                if (data.items[0].items) {
                    if (data.items[0].items.card_settings) {
                        var structKeys = Object.keys(data.items[0].items.card_settings);
    
                        for (var i = 0; i < structKeys.length; i++) {
                            var k = structKeys[i];
                            var str = data.items[0].items.card_settings[parseInt(k)].category_text;
                            
                            if (str) {
                                var hash_str = str.toString();
                                var key_str = hash_str.toLowerCase();
                                var value_str = hash_str.toUpperCase();
                                
                                item_list[key_str.replace(/[^a-zA-Z0-9_-]/g,'_')] = value_str;
                            }
                        }
                        
                        for (var key in item_list) {
                            if (item_list.hasOwnProperty(key)) {
                                list.push({"key":key,"val":item_list[key]});
                            }
                        }
                    }
                }
                return list;
            },
            "hasFilterGroup": function() {
                if (data.getFilterGroup().length > 0) {
                    return true;
                }
                return false;
            }
        }
        return data;
    }
}
