'use strict';
// event.queryParams.objID
const Mustache = require('mustache');
const fs = require('fs');
const config = require('../config/config');

exports.handler = async (event, context) => {
    if (config.environment == 'production') {
        var post_data = JSON.parse(JSON.stringify(event.body));
        var objtemplate_id = post_data[0].objtemplate_id;
    } else {
        var objtemplate_id = config.objtemplate_id;
    }
    var objtemplate_data_and_function = require((config.environment == 'production' ? null : __dirname+ "/") + 'objtemplate_'+ objtemplate_id + '.js');
    var data = objtemplate_data_and_function.renderValues(event.body);
    
    var styles = fs.readFileSync((config.environment == 'production' ? null : __dirname+ "/") + "styles_"+ objtemplate_id +".html").toString();
    var scripts = fs.readFileSync((config.environment == 'production' ? null : __dirname+ "/") + "scripts_"+ objtemplate_id +".js").toString();
    var css_js = {
        "styles" : styles,
        "javascripts": scripts
    }
    var page = fs.readFileSync((config.environment == 'production' ? null : __dirname+ "/") + "page_"+ objtemplate_id +".mustache").toString();


    var output = Mustache.render(page, data, css_js);
    
    if (config.environment == "production") {
        const response = {
            "statusCode": 200,
            "body": output
        };
        return response;
    } else {
        context.send(output);
    }
    
};

